import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1: Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal
        // ArrayList vừa tạo. Sắp xếp các phần tử theo thứ tự giá trị tăng dần và ghi
        // lại ra terminal ArrayList vừa được sắp xếp
        ArrayList<Integer> task1 = new ArrayList<Integer>();
        task1.add(1);
        task1.add(7);
        task1.add(8);
        task1.add(98);
        task1.add(22);
        task1.add(46);
        task1.add(3);
        task1.add(6);
        task1.add(74);
        System.out.println(task1);
        Collections.sort(task1);
        System.out.println("Task 1: " + task1);

        // task 2: Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal
        // ArrayList vừa tạo. Tạo một ArrayList mới chứa các số được lấy từ ArrayList
        // trên có giá trị từ 10 đến 100 và ghi ra terminal ArrayList mới này.
        ArrayList<Integer> task2 = new ArrayList<Integer>();

        for (int number : task1) {
            if (number >= 10 && number <= 100) {
                task2.add(number);
            }
        }
        System.out.println("Task 2: " + task2);

        // task 3: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra
        // terminal ArrayList vừa tạo. Kiểm tra ArrayList này nếu chứa màu vàng thì in
        // ra terminal chữ “OK” còn ngược lại in ra chữ “KO”
        ArrayList<String> task3 = new ArrayList<String>();
        task3.add("Red");
        task3.add("Blue");
        task3.add("Green");
        task3.add("Yellow");
        task3.add("Purple");
        if (task3.contains("Yellow")) {
            System.out.println("Task3 : OK");
        } else {
            System.out.println("Task3 : NO");
        }

        // task 4: Hàm tạo mới ArrayList Integer, thêm 10 số và tính tổng các số
        int sum = 0;
        for (int i : task1) {
            sum = sum + i;
            // sum += i
        }
        System.out.println("Task 4: " + sum);

        // // task 5: Hàm tạo mới ArrayList String, thêm 5 màu sắc và xóa bỏ toàn bộ giá
        task3.clear();
        System.out.println("Task 5: " + task3);

        // task 6: Hoán đổi ngẫu nhiên vị trí các phần tử của ArrayList và ghi lại ra
        ArrayList<String> task6 = new ArrayList<String>();
        task6.add("red");
        task6.add("green");
        task6.add("blue");
        task6.add("yellow");
        task6.add("orange");
        task6.add("pink");
        task6.add("purple");
        task6.add("green");
        task6.add("violet");
        task6.add("black");
        System.out.println("Task 6:");
        System.out.println(task6);
        Collections.shuffle(task6);
        System.out.println("ArrayList sau khi hoán đổi " + task6);

        // task 7: Đảo ngược vị trí các phần tử của ArrayList
        Collections.reverse(task6);
        System.out.println("Task 7: ArrayList sau khi ĐẢO NGƯỢC " + task6);
        // task 8: cắt từ phần tử thứ 3 => 7 của ArrayList
        ArrayList<String> task8 = new ArrayList<String>();
        task8.add("red");
        task8.add("green");
        task8.add("blue");
        task8.add("yellow");
        task8.add("orange");
        task8.add("pink");
        task8.add("purple");
        task8.add("aa");
        task8.add("violet");
        task8.add("black");
        
        System.out.println("Task 8: ArrayList con sau khi cắt " + task8.subList(3, 7));

        // task 9: Hoán đổi vị trí của phần tử thứ 3 với phần tử thứ 7 
        Collections.swap(task8, 3, 7);
        System.out.println("Task 9: ArrayList sau khi đổi vị trí 3 và 7 " + task8);

        // Task 10: Copy một ArrayList vào ArrayList còn lại và ghi lại ra terminal 2 ArrayList này.
        System.out.println("Task 10:");
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        System.out.println("List 1: " + list1);

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(4);
        list2.add(5);
        list2.add(6);
        System.out.println("List 2: " + list2);

        // copy list2 to list1
        Collections.copy(list1, list2);

        System.out.println("After copying List 2 to List 1");
        System.out.println("List 1: " + list1);
        System.out.println("List 2: " + list2);



    }
}
